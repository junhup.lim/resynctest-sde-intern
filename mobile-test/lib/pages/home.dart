import 'package:flutter/material.dart';
import 'package:note_app/config.dart';
import 'dart:async';
import 'package:note_app/services/database.dart';
import 'package:note_app/models/notes_model.dart';
import 'package:note_app/components/notes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key) {}

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<NotesModel> notesList = [];
  List<dynamic> history = [];
  List<Duration> alarms = [];
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;
  DateTime alarmTime;

  @override
  void initState() {
    super.initState();
    NotesDatabaseService.db.init();
    setNotesFromDB();
    initializing();
  }

  setNotesFromDB() async {
    var fetchedNotes = await NotesDatabaseService.db.getNotesFromDB();
    setState(() {
      notesList = fetchedNotes;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(alarms);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(APP_TITLE),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.notifications)),
              Tab(icon: Icon(Icons.alarm)),
              Tab(icon: Icon(Icons.history)),
            ],
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text('Note'),
                onTap: () {},
              ),
              ListTile(
                title: Text('Alarm'),
                onTap: () {},
              ),
              ListTile(
                title: Text('History'),
                onTap: () {},
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            buildNotes(),
            buildAlarm(),
            buildHistory(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            addNote(context);
          },
        ),
      ),
    );
  }

  Widget buildNotes() {
    List<Widget> noteComponentsList = [];
    notesList.sort((a, b) {
      return b.date.compareTo(a.date);
    });

    notesList.forEach((note) {
      noteComponentsList.add(Column(children: <Widget>[
        Row(
          children: [
            Container(
                padding: const EdgeInsets.only(left: 10.0),
                height: 20,
                width: 30,
                child: IconButton(
                  iconSize: 20.0,
                  icon: Icon(Icons.delete),
                  color: const Color(0xff6AC6B0),
                  onPressed: () async {
                    history.insert(0, note);
                    await NotesDatabaseService.db.deleteNoteInDB(note);
                    await setNotesFromDB();
                  },
                )),
            Container(
                padding: const EdgeInsets.only(left: 10.0),
                height: 20,
                width: 30,
                child: IconButton(
                  iconSize: 20.0,
                  icon: Icon(Icons.edit),
                  color: const Color(0xff6AC6B0),
                  onPressed: () async {
                    history.insert(0, note);
                    editNote(note, context);
                  },
                )),
          ],
        ),
        NoteCardComponent(noteData: note),
      ]));
    });

    return SingleChildScrollView(child: Column(children: noteComponentsList));
  }

  Widget buildAlarm() {
    List<Widget> alarmsList = [];

    alarms.forEach((alarm) {
      alarmsList.add(Column(children: <Widget>[
        Row(children: [
          Icon(Icons.alarm),
          Text('$alarm'),
          IconButton(
            iconSize: 20.0,
            icon: Icon(Icons.delete),
            color: const Color(0xff6AC6B0),
            onPressed: () async {
              alarms.remove(alarm);
              history.add(alarm.toString());
              setState(() {});
            },
          ),
          IconButton(
            iconSize: 20.0,
            icon: Icon(Icons.edit),
            color: const Color(0xff6AC6B0),
            onPressed: () async {
              await addAlarm(context);
              setState(() {});
              history.add(alarm.toString());
              alarms.remove(alarm);
            },
          )
        ])
      ]));
    });

    alarmsList.add(FlatButton(
      color: Colors.blue,
      onPressed: () async {
        await addAlarm(context);
        setState(() {});
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "Add",
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
    ));
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: alarmsList,
      ),
    );
  }

  Widget buildHistory() {
    List<Widget> historyList = [];

    history.forEach((elem) {
      historyList.insert(
          0,
          Column(children: <Widget>[
            IconButton(
              iconSize: 20.0,
              icon: Icon(Icons.delete),
              color: const Color(0xff6AC6B0),
              onPressed: () async {
                history.remove(elem);
                setState(() {});
              },
            ),
            elem is NotesModel
                ? NoteCardComponent(noteData: elem)
                : Row(children: [Icon(Icons.alarm), Text(elem)])
          ]));
    });
    return SingleChildScrollView(child: Column(children: historyList));
  }

  addNote(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          TextEditingController noteTitleController = TextEditingController();
          TextEditingController noteBodyController = TextEditingController();
          return AlertDialog(
              title: Text("Add Note"),
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Column(children: <Widget>[
                  TextField(
                      controller: noteTitleController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Title')),
                  TextField(
                      controller: noteBodyController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Body')),
                  Row(children: <Widget>[
                    MaterialButton(
                        elevation: 5.0,
                        child: Text('Add'),
                        onPressed: () async {
                          NotesModel newNote = NotesModel(
                              content: noteBodyController.text,
                              title: noteTitleController.text,
                              date: DateTime.now());
                          await NotesDatabaseService.db.addNoteInDB(newNote);
                          refetchNotesFromDB();
                          Navigator.of(context).pop({});
                        })
                  ])
                ]);
              }));
        });
  }

  editNote(NotesModel note, BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          TextEditingController noteTitleController = TextEditingController();
          TextEditingController noteBodyController = TextEditingController();
          noteTitleController.text = note.title;
          noteBodyController.text = note.content;
          return AlertDialog(
              title: Text("Edit Note"),
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Column(children: <Widget>[
                  TextField(
                      controller: noteTitleController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Title')),
                  TextField(
                      controller: noteBodyController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Body')),
                  Row(children: <Widget>[
                    MaterialButton(
                        elevation: 5.0,
                        child: Text('Add'),
                        onPressed: () async {
                          NotesModel newNote = NotesModel(
                              id: note.id,
                              content: noteBodyController.text,
                              title: noteTitleController.text,
                              date: DateTime.now());
                          await NotesDatabaseService.db.updateNoteInDB(newNote);
                          refetchNotesFromDB();
                          Navigator.of(context).pop({});
                        })
                  ])
                ]);
              }));
        });
  }

  addAlarm(BuildContext context) async {
    DateTime alarmDay;
    Duration duration;
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Text("Add Note"),
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Column(children: <Widget>[
                  hourMinute15Interval(),
                  Row(children: <Widget>[
                    MaterialButton(
                        elevation: 5.0,
                        child: Text('Add'),
                        onPressed: () async {
                          final DateTime picked = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2021));

                          if (picked != null && picked != DateTime.now()) {
                            setState(() {
                              alarmDay = picked;
                            });
                          }
                          DateTime updated = alarmDay.add(Duration(
                              hours: alarmTime.hour,
                              minutes: alarmTime.minute));

                          setState(() {
                            duration = updated.difference(DateTime.now());
                          });
                          alarms.insert(0, duration);
                          _showNotificationsAfterDuration(duration);
                          Navigator.of(context).pop({});
                        })
                  ])
                ]);
              }));
        });
  }

  Widget hourMinute15Interval() {
    return TimePickerSpinner(
      spacing: 40,
      minutesInterval: 15,
      onTimeChange: (time) {
        setState(() {
          alarmTime = time;
        });
      },
    );
  }

  void refetchNotesFromDB() async {
    await setNotesFromDB();
    print("Refetched notes");
  }

  void initializing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  void _showNotifications() async {
    print('clicked');
    await notification();
  }

  void _showNotificationsAfterDuration(Duration duration) async {
    await notificationAfterDuration(duration);
  }

  Future<void> notification() async {
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            'Channel ID', 'Channel title', 'channel body',
            priority: Priority.High,
            importance: Importance.Max,
            ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0, 'Hello there', 'ALARM!', notificationDetails);
  }

  Future<void> notificationAfterDuration(Duration duration) async {
    var timeDelayed = DateTime.now().add(duration);
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            'second channel ID', 'second Channel title', 'second channel body',
            priority: Priority.High,
            importance: Importance.Max,
            ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.schedule(1, 'Hello there',
        'please subscribe my channel', timeDelayed, notificationDetails);
  }

  Future onSelectNotification(String payLoad) {
    if (payLoad != null) {
      print(payLoad);
    }

    // we can set navigator to navigate another screen
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }
}
